classnote
=========

`classnote` is a KISS-style note taking application,
targetted at terminal users. It uses your preferred `EDITOR`
to write notes and divide them into categories.

`classnote` stores your notes as plaintext .md files and
commits them into a `git` tree.

Example usage
=============

    classnote new

opens up your editor and starts writing a note. When saved,
`classnote` puts your note in the directory specified in the
config file (per default: `~/Documents/Notes/`).

Config file
===========

The config file resides at `~/.config/classnote/classnote.conf`,
per the xdg-specs, and looks as follows:

    [default]
    path = /home/rsmet/Documents/Notes/

It's basically an ini file that contains the categories and
subcategories of your hierarchy of notes, together with paths.

A more complex config file can look as follows:

    [default]
    path = /home/rsmet/Documents/Notes/
    
    [school]
    path = /home/rsmet/Documents/Ba3/Notes/
    
    [school/SoftwareEngineering]
    path = /home/rsmet/Documents/Ba3/SoftwareEngineering/Notes

More advanced usage
===================

With respect to the above config, you can use

    classnote -c school/Soft new

to create a new note in the school/SoftwareEngineering category.
