#!/usr/bin/env python3

from setuptools import setup

setup(name='classnote',
      version='0.1.0',
      description='',
      author='Ruben De Smet',
      author_email='ruben.de.smet@glycos.org',
      py_modules=['classnote'],
      packages=['classnote'],
      license='GPLv3+',
      entry_points={
          'console_scripts': [
              'classnote = classnote.main:main'
          ]
      },
      install_requires=["appdirs", "gitpython"]
)
