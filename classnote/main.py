#!/usr/bin/env python3

import sys
import argparse
from . import classnote
import sys, tempfile, os
import time
from subprocess import call

def get_cat(notebook, args):
    if args.category:
        category = notebook.find_category(args.category)
        if category is None:
            print("Category '%s' not configured" % args.category)
            sys.exit(-1)
    else:
        category = list(notebook.categories().values())[0]
    return category

def browse(notebook, args, sublevel = 0):
    if sublevel == 0:
        print("Categories: ")
        categories = notebook.categories()
    else:
        categories = notebook

    indentation = " " * sublevel
    for name, category in categories.items():
        print("%s- %s" % (indentation, category.name))
        if category.has_subcategories():
            browse(category.categories, args, sublevel + 1)

def list_notes(notebook, args):
    category = get_cat(notebook, args)
    print("Notes in '%s' notebook:" % category.name)

def new_note(notebook, args):
    category = get_cat(notebook, args)

    EDITOR = os.environ.get('EDITOR','vim') #that easy!

    initial_message = "Notes from %s" % (time.strftime("%a, %d %b %Y %H:%M:%S"))

    with tempfile.NamedTemporaryFile(suffix=".md") as tf:
        tf.write(initial_message.encode())
        tf.flush()
        call([EDITOR, tf.name])

        commitmsg = initial_message + " for " + category.name
        category.add_note(commitmsg, tf.name)

def new_category(notebook, args):
    path = args.path
    (pcategory,subcategory) = args.category.rsplit("/", 1)
    parent = get_cat(pcategory)
    parent.create_category(subcategory, path)


def main():
    parser = argparse.ArgumentParser()
    subcommand = parser.add_subparsers(title='subcommands', dest='subcommand')

    newparser = subcommand.add_parser("new", help="new help", aliases=['n'])
    newparser.set_defaults(func=new_note)
    newparser.add_argument("--category", '-c', dest='category')

    listparser = subcommand.add_parser("list", help="list", aliases=['l', 'ls'])
    listparser.set_defaults(func=list_notes)
    listparser.add_argument("--category", '-c', dest='category')

    newcatparser = subcommand.add_parser("new-category", help="Create a new category", aliases=['nc'])
    newcatparser.set_defaults(func=new_category)
    newcatparser.add_argument("category", type=str)
    newcatparser.add_argument("path", type=str)

    args = parser.parse_args()

    notebook = classnote.notebook()
    notebook.load()

    if hasattr(args, "func"):
        args.func(notebook, args)
    else:
        browse(notebook, args)
