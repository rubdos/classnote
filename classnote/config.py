#!/usr/bin/env python3

import configparser
import os
import appdirs

class config:
    def load(self):
        self.config = configparser.ConfigParser()

        if not os.path.isfile(self.get_path()):
            self.create_new_config()
        else:
            self.config.read(self.get_path())

    def categories(self):
        return self.config.sections()

    def create_new_config(self):
        self.config["default"] = {
            "path": os.path.join(appdirs.user_data_dir("classnote", "Ruben De Smet")
                , "/Documents/Notes/")
        }
        with open(self.get_path(), "w") as f:
            self.config.write(f)
    def get_path(self):
        cfgdir = appdirs.user_config_dir("classnote", "Ruben De Smet")
        if not os.path.isdir(cfgdir):
            os.mkdir(cfgdir)
        return os.path.join(cfgdir, "classnote.conf")

    def __getitem__(self, index):
        return self.config.__getitem__(index)
