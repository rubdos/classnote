#!/usr/bin/env python3

from .config import config
import git
import os
import shutil
import time

class notebook:
    def load(self):
        self.configuration = config()
        self.configuration.load()
        self.load_index()

    def categories(self):
        return self.index

    def find_category(self, path):
        current = self.categories().values()
        dead = False

        level = 0
        splitted = path.split("/")

        while not dead:
            dead = True
            for cat in current:
                if cat.name.startswith(splitted[level]):
                    dead = False
                    current = cat
                    level += 1
                    if level == len(splitted):
                        return current
        return None

    def load_index(self):
        self.index = {}

        for cat in sorted(self.configuration.categories(), 
                key=lambda c: c.count("/")):
            splitted = cat.split("/")

            if len(splitted) == 1:
                self.index[cat] = category(cat, self.configuration[cat])
            else:
                current = self.index[splitted[0]]
                for i in range(1, len(splitted) - 1):
                    current = current[splitted[i]]
                current.add_subcategory(splitted[-1], category(splitted[-1], self.configuration[cat]))

class category:
    def __init__(self, name, config):
        self.config = config
        self.name = name
        self.categories = {}
        if not os.path.isdir(self.config["path"]):
            os.mkdir(self.config["path"])
        try:
            self.repo = git.Repo(self.config["path"])
        except git.exc.InvalidGitRepositoryError:
            self.repo = git.Repo.init(self.config["path"])

    def add_subcategory(self, name, cat):
        self.categories[name] = cat

    def has_subcategories(self):
        return len(self.categories) > 0

    def add_note(self, title, notefile):
        newname = time.strftime("%Y%m%d%H%M%S")
        shutil.copy(notefile, os.path.join(self.config["path"], newname + ".md"))
        self.repo.index.add([newname + ".md"])
        self.repo.index.commit(title)

    def __getitem__(self, index):
        self.categories[index]
    def __iter__(self):
        return self.categories.values().__iter__()
